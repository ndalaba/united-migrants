dump:	wp-cli.phar
	php wp-cli.phar search-replace "http://localhost:8000" "https://lalya-notaire.com"  --export=dump.sql

import: wp-cli.phar
	php wp-cli.phar db import dump.sql

wp-cli.phar:
	curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar