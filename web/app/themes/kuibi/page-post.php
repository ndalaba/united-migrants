<?php
/*
        Template Name: Actualites
    */
?>
<?php get_header() ?>

<?php while (have_posts()) : the_post(); ?>
    <?php
    $slides = ['slide1.jpg', "slide2.jpg", "slide3.jpg", "slide4.jpg"];
    $slide = $slides[rand(0, 3)];
    ?>
    <div class="nav-backed-header parallax" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/<?= $slide ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li> <a href="<?= bloginfo("url") ?>">Accueil</a></li>
                        <li> <a href="#">Actualités</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-8">
                    <h1><?= the_title() ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 contenu">
                        <?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                        $blogs = new WP_Query(['post_type' => 'post', 'category_name' => 'actualites',  'post_status' => 'publish', 'posts_per_page' => 6, 'paged' => $paged]);
                        ?>
                        <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
                            <article class="post-content cause-item">
                                <h2 class="post-title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h2>
                                <?php if (has_post_thumbnail()) : ?>
                                    <a href="<?= the_permalink() ?>" style="height: 230px;overflow: hidden;display: inline-block;"><img src="<?= the_post_thumbnail_url() ?>" class="img-responsive"></a>
                                <?php endif; ?>
                                <span class="meta-data">
                                    <span> <i class="fa fa-calendar"></i> <?= the_time('d F Y') ?></span>
                                    <span> <a href="#"><i class="fa fa-user"></i><?= the_author_meta("nicename") ?></a>
                                    </span>
                                </span>
                                <div class="spacer-30"></div>
                                <?= the_excerpt() ?>
                            </article>
                        <?php endwhile; ?>
                        <?php $pagination = pagination_bar($blogs);
                        if (!empty($pagination)) : ?>
                            <ul class="pagination">
                                <?php foreach ($pagination as $key => $page_link) : ?>
                                    <li class="<?php if (strpos($page_link, 'current') !== false) {
                                                    echo ' active';
                                                } ?>"><span><?= $page_link ?></span></li>
                                <?php endforeach ?>
                            </ul>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <div class="col-md-4">
                        <?php get_template_part("templates/side") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_footer() ?>