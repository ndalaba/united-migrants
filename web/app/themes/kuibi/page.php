<?php get_header() ?>

<?php while (have_posts()) : the_post(); ?>
    <?php
    $slides = ['slide1.jpg', "slide2.jpg","slide3.jpg","slide4.jpg"];
    $slide = $slides[rand(0, 3)];
    ?>
    <div class="nav-backed-header parallax" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/<?= $slide ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li> <a href="<?= bloginfo("url") ?>">Accueil</a></li>
                        <li> <a href="#"><?= the_title() ?></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-8">
                    <h1><?= the_title() ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 contenu">
                        <article class="post-content cause-item">
                            <?php if (has_post_thumbnail()) : ?>
                                <img src="<?= the_post_thumbnail_url() ?>" class="img-responsive">
                            <?php endif; ?>
                            <div class="spacer-30"></div>
                            <?= the_content() ?>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_template_part("templates/galerie") ?>
<?php get_footer() ?>