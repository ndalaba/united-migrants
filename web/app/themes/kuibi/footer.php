<?php get_template_part("inc/footer") ?>

<a id="back-to-top">
<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-arrow-up-circle" width="30" height="30" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
  <path stroke="none" d="M0 0h24v24H0z" fill="none"/>
  <circle cx="12" cy="12" r="9" />
  <line x1="12" y1="8" x2="8" y2="12" />
  <line x1="12" y1="8" x2="12" y2="16" />
  <line x1="16" y1="12" x2="12" y2="8" />
</svg>
</a>
</div>
<script src="<?= get_template_directory_uri() . '/assets/js/jquery-2.0.0.min.js' ?>"></script> <!-- Jquery Library Call --> 
<script src="<?= get_template_directory_uri() . '/assets/plugins/prettyphoto/js/prettyphoto.js' ?>"></script> <!-- PrettyPhoto Plugin --> 
<script src="<?= get_template_directory_uri() . '/assets/js/helper-plugins.js' ?>"></script> <!-- Plugins --> 
<script src="<?= get_template_directory_uri() . '/assets/js/bootstrap.js' ?>"></script> <!-- UI --> 
<script src="<?= get_template_directory_uri() . '/assets/js/waypoints.js' ?>"></script> <!-- Waypoints --> 
<script src="<?= get_template_directory_uri() . '/assets/plugins/mediaelement/mediaelement-and-player.min.js' ?>"></script> <!-- MediaElements --> 
<script src="<?= get_template_directory_uri() . '/assets/js/init.js' ?>"></script> <!-- All Scripts --> 
<script src="<?= get_template_directory_uri() . '/assets/plugins/flexslider/js/jquery.flexslider.js' ?>"></script> <!-- FlexSlider --> 
<script src="<?= get_template_directory_uri() . '/assets/plugins/countdown/js/jquery.countdown.min.js' ?>"></script> <!-- Jquery Timer -->

<?php wp_footer() ?>

</body>

</html>