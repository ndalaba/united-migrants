<?php $blogs = new WP_Query(['post_type' => 'post', 'category_name' => 'faq', 'post_status' => 'publish', 'posts_per_page' => 3]); ?>
<?php if ($blogs->have_posts()) : ?>
   <div class="row">
      <div class="col-xl-6 mx-auto text-center">
         <header class="listing-header">
            <h3>FAQ</h3>
         </header>
      </div>
   </div>
   <div class="row" style="    background-color: #f8f7f3;    padding: 16px 0;">
      <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
         <div class="col-lg-4 col-md-6">
            <div class="single-blog">
               <div class="blog-content">
                  <div class="blog-title">
                     <h4><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h4>
                  </div>
                  <a href="<?= the_permalink() ?>" class="box_btn"><?= substr(get_the_excerpt(), 0, 170) ?>...</a>
               </div>
            </div>
         </div>
      <?php endwhile; ?>
   </div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>