<div class="listing sermons-listing">
   <header class="listing-header">

      <h3>Vidéos</h3>

   </header>
   <section class="listing-cont">
      <ul>
         <?php $blogs = new WP_Query(['post_type' => 'video', 'post_status' => 'publish', 'posts_per_page' => 1]); ?>
         <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
            <li class="item sermon featured-sermon"> <span class="date"><?= the_time('l, d F Y') ?></span>
               <h5><a href="<?= the_permalink() ?>" style="color:#de7219"><?= the_title() ?></a></h5>
               <div class="featured-sermon-video">
                  <iframe width="200" height="150" src="https://www.youtube.com/embed/<?= the_field('id_video') ?>"></iframe>
               </div>
               <?= the_excerpt() ?>
            </li>
         <?php endwhile;
         wp_reset_postdata(); ?>
      </ul>

   </section>

</div>