<?php $filter = [
   [
      'key' => 'date',
      'value' => date('Y-m-d'),
      'type' => 'date',
      'compare' => '>='
   ]
]; ?>
<?php $blogs = new WP_Query(['post_type' => 'post', 'category_name' => 'evenements', 'post_status' => 'publish', 'posts_per_page' => 3,"meta_query"=>$filter]); ?>
<?php if ($blogs->have_posts()) : ?>
   <div class="listing events-listing">
      <header class="listing-header">
         <h3>Les évènements de <?= bloginfo("title") ?></h3>
      </header>

      <section class="listing-cont">
         <ul>
            <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
               <li class="item event-item">
                  <div class="event-detail">
                     <h4><a href="<?= the_permalink() ?>"><?php the_title() ?></a></h4>
                     <span class="event-dayntime meta-data"><?= the_date("d") ?> <?= get_the_date("M") ?></span>
                  </div>
                  <div class="to-event-url">
                     <div><a href="<?= the_permalink() ?>" class="btn btn-default btn-sm">+ d'infos</a></div>
                  </div>
               </li>
            <?php endwhile; ?>
         </ul>
      </section>
   </div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>