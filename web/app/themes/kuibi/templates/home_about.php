<section class="about__area-2 pt-120 pb-120 box-105 p-relative">
    <div class="container-fluid">
        <div class="about__inner about__inner-3 p-relative">
            <div class="about__bg-img p-absolute" data-background="<?= get_template_directory_uri() ?>/assets/images/aider.gif"></div>
            <div class="row">
                <div class="col-xl-6 offset-xl-6">
                    <div class="about__content-wrapper pt-20 pl-85">
                        <div class="section__title mb-30">
                            <div class="section__icon mb-10">
                                <span class="section__sub-title section__sub-title-2"><?= bloginfo("title") ?></span>
                            </div>
                            <h1 class="mb-100">Association de demandeurs d’asile et migrants sans papiers de toutes nationalités, sans distinction d’appartenance religieuse ou politique.</h1>
                        </div>
                        <div class="about__info-2 d-sm-flex mb-60">
                            <div class="about__info-experience white-bg mr-40">
                                <h1>20 années d'existance</h1>
                            </div>
                            <p><?= bloginfo("title") ?> a pour but de manifester une solidarité active avec les personnes opprimées et exploitées. Elle défend la dignité et les droits des personnes réfugiées et migrantes, quelles que soient leurs origines, leurs opinions politiques ou leurs convictions.</p>
                        </div>
                        <div class="about-bottom d-sm-flex">
                            <div class="about-author mr-30 mb-30">
                                <img src="assets/img/about/author.png" alt="">
                                <div class="ab-author">
                                    <h3>Dalian Machen</h3>
                                    <span>CEO, Sycho</span>
                                </div>
                            </div>
                            <div class="about-btn mr-40 mb-30">
                                <a href="<?= site_url('/qui-sommes-nous') ?>" class="s-btn s-btn__square s-btn__square-2" tabindex="0">En savoir plus</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>