<?php $blogs = new WP_Query(['post_type' => 'galerie', 'post_status' => 'publish', 'posts_per_page' => 3]); ?>
<?php if ($blogs->have_posts()) : ?>
  <div class="featured-gallery">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-3">
          <h4>Galerie</h4>
          <a href="#" class="btn btn-default btn-lg">Plus</a>
        </div>
        <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
          <div class="col-md-3 col-sm-3 post format-image">
            <a href="<?= the_post_thumbnail_url() ?>" class="media-box" data-rel="prettyPhoto[Gallery]" style="height: 150px; overflow: hidden;">
              <img src="<?= the_post_thumbnail_url() ?>" alt="<?= the_title() ?>" >
            </a>
          </div>
        <?php endwhile; ?>
      </div>
    </div>
  </div>
<?php endif; ?>
<?php wp_reset_postdata(); ?>