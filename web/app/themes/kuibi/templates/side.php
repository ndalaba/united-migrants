<div class="widget sidebar-widget">
   <div class="sidebar-widget-title">
      <h3>S'informer</h3>
   </div>
   <ul>
      <li><a href="<?= site_url('/actualites') ?>" style="color: #4c4b4b;">S'informer</a></li>
      <li><a href="<?= site_url('/evenements') ?>" style="color: #4c4b4b;">Agenda</a></li>
      <li><a href="<?= site_url('/faq') ?>" style="color: #4c4b4b;">FAQ</a></li>
   </ul>
</div>
<?php get_template_part("templates/video") ?>