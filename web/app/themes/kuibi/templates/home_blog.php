<div class="row" style="background-color: #f8f7f3;padding-top: 10px;">
    <header class="listing-header">
        <h3>À la une</h3>
    </header>
    <div class="featured-blocks clearfix">
        <?php $blogs = new WP_Query(['post_type' => 'post', 'meta_key' => "top", 'meta_value' => true, 'post_status' => 'publish', 'posts_per_page' => 3]); ?>
        <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
            <div class="col-md-4 col-sm-4 featured-block">
                <a href="<?= the_permalink() ?>" class="img-thumbnail">
                    <img src="<?= the_post_thumbnail_url() ?>" alt="<?php the_title() ?>" style="height: 200px;">
                    <strong><?= substr(the_title('', '', false), 0, 170) ?>...</strong> <span class="more">Voir</span>
                </a>
            </div>
        <?php endwhile;
        wp_reset_postdata(); ?>
    </div>
    <!-- End Featured Blocks -->
</div>