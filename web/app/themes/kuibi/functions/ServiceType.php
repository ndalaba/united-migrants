<?php

class ServiceType{

    public static function register(){
        add_action('init', [self::class,'postType'], 0);
    }

    public static function postType(){
        $labels = array(
            'name'                  => _x('Services', 'Post Type General Name', 'text_domain'),
            'singular_name'         => _x('Service', 'Post Type Singular Name', 'text_domain'),
            'menu_name'             => __('Service', 'text_domain'),
            'name_admin_bar'        => __('Service', 'text_domain'),
            'archives'              => __('Nos services', 'text_domain'),
            'attributes'            => __('', 'text_domain'),
            'parent_item_colon'     => __('', 'text_domain'),
            'all_items'             => __('Tous nos services', 'text_domain'),
            'add_new_item'          => __('Ajouter', 'text_domain'),
            'add_new'               => __('Ajouter', 'text_domain'),
            'new_item'              => __('Nouveau service', 'text_domain'),
        );
        $args = array(
            'label'                 => __('Service', 'text_domain'),
            'description'           => __('Nos services', 'text_domain'),
            'labels'                => $labels,
            'supports'              => ['title', 'editor', 'thumbnail'],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 18,
            'menu_icon'             => 'dashicons-megaphone',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
        );
        register_post_type('service', $args);
    }
}