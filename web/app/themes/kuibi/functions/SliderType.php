<?php


class SliderType{

    public static function register(){
        add_action('init', [self::class,'postType'], 0);              
    }

    public static function postType(){
        $labels = array(
            'name'                  => _x('Sliders', 'Post Type General Name', 'text_domain'),
            'singular_name'         => _x('Slider', 'Post Type Singular Name', 'text_domain'),
            'menu_name'             => __('Slider', 'text_domain'),
            'name_admin_bar'        => __('Slider', 'text_domain'),
            'archives'              => __('Notre slider', 'text_domain'),
            'attributes'            => __('', 'text_domain'),
            'parent_item_colon'     => __('', 'text_domain'),
            'all_items'             => __('Notre slider', 'text_domain'),
            'add_new_item'          => __('Ajouter', 'text_domain'),
            'add_new'               => __('Ajouter', 'text_domain'),
            'new_item'              => __('Nouveau', 'text_domain'),
        );
        $args = array(
            'label'                 => __('Slider', 'text_domain'),
            'description'           => __('Notre sliders', 'text_domain'),
            'labels'                => $labels,
            'supports'              => ['title','excerpt', 'thumbnail'],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 16,
            'menu_icon'             => 'dashicons-format-gallery',
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'capability_type'       => 'post',
        );
        register_post_type('slider', $args);
    }
}