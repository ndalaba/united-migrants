<header class="site-header">
	<div class="topbar">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-sm-9 col-xs-9">
					<h1 class="logo">
						<img class="visible-md visible-lg" src="<?= get_template_directory_uri() ?>/assets/images/logo_footer.png" alt="<?= bloginfo("title") ?> ">
						<img class="visible-sm visible-xs" src="<?= get_template_directory_uri() ?>/assets/images/logo_white.png" alt="<?= bloginfo("title") ?> ">
						<?= bloginfo("title") ?>
					</h1>
					<p class="corporate">Association de demandeurs d’asile et migrants sans papiers</p>
				</div>

				<div class="col-md-4 col-sm-3 col-xs-3">
					<ul class="top-navigation hidden-sm hidden-xs">
						<li><span><i class="fa fa-envelope"></i> <?= get_option('email') ?></span></li>
						<li><span><i class="fa fa-phone"></i> <?= get_option('phone') ?></span></li>
					</ul>
					<a href="#" class="visible-sm visible-xs menu-toggle"><i class="fa fa-bars"></i></a>
				</div>

			</div>
		</div>
	</div>

	<div class="main-menu-wrapper">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<?php if (has_nav_menu('header')) : ?>
						<?php wp_nav_menu(['theme_location' => "header", 'container_class' => 'navigation', 'container_id' => '', 'menu_id' => 'menu-main-menu', 'menu_class' => 'sf-menu']) ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</header>