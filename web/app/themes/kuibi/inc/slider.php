<div class="hero-slider flexslider clearfix" data-autoplay="yes" data-pagination="yes" data-arrows="yes" data-style="fade" data-pause="yes">
  <ul class="slides">
    <?php $sliders = new WP_Query(['post_type' => 'slider', 'post_status' => 'publish']); ?>
    <?php while ($sliders->have_posts()) : $sliders->the_post(); ?>
      <li class="parallax" style="background-image:url(<?= the_post_thumbnail_url() ?>);"></li>
    <?php endwhile;
    wp_reset_postdata(); ?>
  </ul>
</div>