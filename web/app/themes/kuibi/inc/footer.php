<footer class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-4 widget footer-widget">
				<h4 class="footer-widget-title">A propos</h4>
				<img src="<?= get_template_directory_uri() ?>/assets/images/logo_footer.png" alt="<?= bloginfo("title") ?> ">
				<div class="spacer-20"></div>
				<p><?= get_option("description") ?></p>
			</div>
			<div class="col-md-4 col-sm-4 widget footer-widget">
				<h4 class="footer-widget-title">Informations</h4>
				<ul>
					<li><a href="<?= site_url('/') ?>">Accueil</a></li>
					<li><a href="<?= site_url('/united-migrant') ?>">A propos</a></li>
					<li><a href="<?= site_url('/actualites') ?>">S'informer</a></li>
					<li><a href="<?= site_url('/agenda') ?>">Agenda</a></li>
					<li><a href="<?= site_url('/faq') ?>">FAQ</a></li>
					<li><a href="<?= site_url('/nous-contacter') ?>">Nous contacter</a></li>
				</ul>
			</div>
			<div class="col-md-4 col-sm-4 widget footer-widget">
				<div class="post_newsletter" id="post_newsletter">
					<h4 class="footer-widget-title">Adresse</h4>
					<div class="footer__widget-content">
						<div class="footer__contact-info">
							<ul>
								<li>
									<div class="footer__contact-address">
										<span><?= get_option("address") ?></span>
									</div>
								</li>
								<li>
									<div class="footer__contact-item">
										<h6>Email: <a href="#"><?= get_option("email") ?></a>
										</h6>
									</div>
								</li>
								<li>
									<div class="footer__contact-item">
										<h6>Tel: <a><?= get_option("phone") ?></a> </h6>
									</div>
								</li>
								<li>
									<div class="footer__contact-item">
										<h6>Web: <a>www.united-migrants.org</a> </h6>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<br class="cl_both">
			</div>

		</div>
	</div>
</footer>

<footer class="site-footer-bottom">
	<div class="container">
		<div class="row">
			<div class="copyrights-col-left col-md-6 col-sm-6">
				<p>Copyright © <?= date('Y') ?> tous droits reservés. <a href="#"> <?= bloginfo("title") ?></a></p>
			</div>
			<div class="copyrights-col-right col-md-6 col-sm-6">
				<div class="social-icons">
					<a href="<?= get_option('facebook') ?>" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-facebook" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
							<path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
							<path d="M7 10v4h3v7h4v-7h3l1 -4h-4v-2a1 1 0 0 1 1 -1h3v-4h-3a5 5 0 0 0 -5 5v2h-3"></path>
						</svg>
					</a>
					<a href="<?= get_option('twitter') ?>" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-twitter" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
							<path stroke="none" d="M0 0h24v24H0z" fill="none" />
							<path d="M22 4.01c-1 .49 -1.98 .689 -3 .99c-1.121 -1.265 -2.783 -1.335 -4.38 -.737s-2.643 2.06 -2.62 3.737v1c-3.245 .083 -6.135 -1.395 -8 -4c0 0 -4.182 7.433 4 11c-1.872 1.247 -3.739 2.088 -6 2c3.308 1.803 6.913 2.423 10.034 1.517c3.58 -1.04 6.522 -3.723 7.651 -7.742a13.84 13.84 0 0 0 .497 -3.753c-.002 -.249 1.51 -2.772 1.818 -4.013z" />
						</svg>
					</a>
					</a>
					<a href="<?= get_option('linkedin') ?>" target="_blank">
						<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-linkedin" width="20" height="20" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
							<path stroke="none" d="M0 0h24v24H0z" fill="none" />
							<rect x="4" y="4" width="16" height="16" rx="2" />
							<line x1="8" y1="11" x2="8" y2="16" />
							<line x1="8" y1="8" x2="8" y2="8.01" />
							<line x1="12" y1="16" x2="12" y2="11" />
							<path d="M16 16v-3a2 2 0 0 0 -4 0" />
						</svg>
					</a>

				</div>
			</div>
		</div>
	</div>
</footer>