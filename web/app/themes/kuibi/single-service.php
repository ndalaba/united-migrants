<?php get_header() ?>
<?php while (have_posts()) : the_post(); ?>
    <?php
    $slides = ['slide1.jpg', "slide2.jpg", "slide3.jpg", "slide4.jpg"];
    $slide = $slides[rand(0, 3)];
    ?>
    <div class="nav-backed-header parallax" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/<?= $slide ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li> <a href="<?= bloginfo("url") ?>">Accueil</a></li>
                        <li> <a href="#">Nos missions</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-8">
                    <h1><?= the_title() ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 contenu">
                        <article class="post-content cause-item">
                            <?php if (has_post_thumbnail()) : ?>
                                <img src="<?= the_post_thumbnail_url() ?>" class="img-responsive">
                            <?php endif; ?>
                            <div class="spacer-30"></div>
                            <?= the_content() ?>
                        </article>
                    </div>
                    <?php endwhile;
                wp_reset_postdata(); ?>
                    <div class="col-md-3">
                        <div class="widget sidebar-widget">

                            <div class="sidebar-widget-title">
                                <h3>Nos missions</h3>
                            </div>
                            <ul>
                                <?php $services = new WP_Query(['post_type' => 'service', 'post_status' => 'publish']); ?>
                                <?php if ($services->have_posts()) : ?>
                                    <?php while ($services->have_posts()) : $services->the_post(); ?>
                                        <li><a href="<?= the_permalink() ?>" title="<?= the_title() ?>" style="color: #4c4b4b;"><?= the_title() ?></a></li>
                                <?php endwhile;
                                endif;
                                wp_reset_postdata(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php get_footer() ?>