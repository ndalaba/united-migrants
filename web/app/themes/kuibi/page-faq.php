<?php
/*
        Template Name: FAQ
    */
?>
<?php get_header() ?>

<?php while (have_posts()) : the_post(); ?>
    <?php
    $slides = ['slide1.jpg', "slide2.jpg", "slide3.jpg", "slide4.jpg"];
    $slide = $slides[rand(0, 3)];
    ?>
    <div class="nav-backed-header parallax" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/<?= $slide ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li> <a href="<?= bloginfo("url") ?>">Accueil</a></li>
                        <li> <a href="#">FAQ</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-8">
                    <h1>Foire à questions</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 contenu">
                        <?php $blogs = new WP_Query(['post_type' => 'post', 'category_name' => 'faq', 'post_status' => 'publish', 'posts_per_page' => 3]); ?>
                        <?php if ($blogs->have_posts()) : ?>
                            <div class="accordion" id="accordionArea">
                                <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
                                    <div class="accordion-group panel">
                                        <div class="accordion-heading accordionize">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordionArea" href="#<?= the_ID() ?>Area"> <?= the_title() ?> <i class="fa fa-angle-down"></i> </a>
                                        </div>
                                        <div id="<?= the_ID() ?>Area" class="accordion-body collapse">
                                            <div class="accordion-inner">
                                                <a href="<?= the_permalink() ?>"><?= the_excerpt() ?></a>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            </div>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <div class="col-md-4">
                        <?php get_template_part("templates/side") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_footer() ?>