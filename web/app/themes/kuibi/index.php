<?php
/*
        Template Name: Home
    */
?>
<?php get_header() ?>
<?php get_template_part('inc/slider') ?>

<div class="notice-bar">
   <div class="container">
      <div class="row">
         <div class="col-md-3 col-sm-6 col-xs-6 notice-bar-title"> <span class="notice-bar-title-icon hidden-xs"><i class="fa fa-google-wallet fa-3x"></i></span> <span class="title-note">Don</span> <strong>Humanitaire</strong> </div>
         <div class="col-md-6 col-sm-6 col-xs-6 notice-bar-event-title">
            <h3 style="margin: 0;"><a href="#">Soutenez notre association</a></h3>
            <span class="meta-data">Ne passons pas à côté de l'humanité</span>
         </div>
         <div class="col-md-2 col-sm-6 hidden-xs"> <a href="#" class="btn btn-primary btn-lg btn-block">Faire un don</a> </div>
      </div>
   </div>
</div>

<div class="main" role="main">
   <div id="content" class="content full">
      <div class="container">
         <?php get_template_part("templates/home_blog") ?>
         <div class="spacer-30"></div>

         <div class="row" style="background-color: #f8f7f3;  padding: 10px;">
            <div class="col-md-8 col-sm-6">
               <?php get_template_part("templates/home_event") ?>

               <div class="spacer-30"></div>

               <!-- Latest News -->
               <div class="listing post-listing">

                  <header class="listing-header">
                     <h3>Derniers billets postés</h3>
                  </header>

                  <section class="listing-cont">
                     <ul>
                        <?php $blogs = new WP_Query(['post_type' => 'post', 'category_name' => 'actualites', 'post_status' => 'publish', 'posts_per_page' => 3]); ?>
                        <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
                           <li class="item post">
                              <div class="row">
                                 <div class="col-md-4">
                                    <a href="<?= the_permalink() ?>" class="media-box" style="height: 130px; overflow: hidden;">
                                       <img src="<?= the_post_thumbnail_url() ?>" alt="<?php the_title() ?>" class="img-thumbnail">
                                    </a>
                                 </div>
                                 <div class="col-md-8">
                                    <div class="post-title">
                                       <h2><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h2>
                                       <span class="meta-data"><i class="fa fa-calendar"></i> <?= the_time('d F Y') ?></span>
                                    </div>
                                 </div>
                              </div>
                           </li>
                        <?php endwhile;
                        wp_reset_postdata(); ?>
                     </ul>
                  </section>
               </div>
            </div>
            <div class="col-md-4 col-sm-6" style="    background-color: white;    padding-top: 10px;">
               <!-- Latest Sermons -->
               <div class="listing sermons-listing">
                  <header class="listing-header">

                     <h3>Vidéos</h3>

                  </header>
                  <section class="listing-cont">
                     <ul>
                        <?php $blogs = new WP_Query(['post_type' => 'video', 'post_status' => 'publish', 'posts_per_page' => 1]); ?>
                        <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
                           <li class="item sermon featured-sermon"> <span class="date"><?= the_time('l, d F Y') ?></span>
                              <h5><a href="<?= the_permalink() ?>" style="color:#de7219"><?= the_title() ?></a></h5>
                              <div class="featured-sermon-video">
                                 <iframe width="200" height="150" src="https://www.youtube.com/embed/<?= the_field('id_video') ?>"></iframe>
                              </div>
                              <?= the_excerpt() ?>
                           </li>
                        <?php endwhile;
                        wp_reset_postdata(); ?>
                     </ul>

                  </section>
                  
               </div>
            </div>
         </div>
         <div class="spacer-30"></div>
         <?php get_template_part("templates/faq") ?>
      </div>
   </div>
</div>

<?php get_template_part("templates/galerie") ?>


<?php get_footer() ?>