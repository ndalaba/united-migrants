<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Diallo Mamadou N'Dalaba">
	<?php wp_head(); ?>
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri() . '/assets/images/logo_white.png' ?>" sizes="16x16">
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;1,100;1,200;1,300;1,400&display=swap" rel="stylesheet">

	<link href="<?= get_template_directory_uri() . '/assets/css/bootstrap.css' ?>" rel="stylesheet" type="text/css">
	<link href="<?= get_template_directory_uri() . '/assets/plugins/mediaelement/mediaelementplayer.css' ?>" rel="stylesheet" type="text/css">
	<link href="<?= get_template_directory_uri() . '/assets/css/style.css' ?>" rel="stylesheet" type="text/css">
	<link href="<?= get_template_directory_uri() . '/assets/plugins/prettyphoto/css/prettyPhoto.css' ?>" rel="stylesheet" type="text/css">
	<!--[if lte IE 8]><link rel="stylesheet" type="text/css" href="css/ie8.css" media="screen" /><![endif]-->
	<!-- Color Style -->
	<link href="<?= get_template_directory_uri() . '/assets/css/color1.css' ?>" rel="stylesheet" type="text/css">
	<link href=<?= get_template_directory_uri() . '/assets/css/custom.css' ?>" rel="stylesheet" type="text/css">
	<link href="<?= get_template_directory_uri() . '/assets/css/print.css' ?>" rel="stylesheet" type="text/css" media="print" />
	<!-- SCRIPTS
		================================================== -->
	<script src="<?= get_template_directory_uri() . '/assets/js/modernizr.js' ?>"></script>

</head>

<body>
	<div class="body">
		<?php get_template_part('inc/header') ?>