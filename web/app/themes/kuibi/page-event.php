<?php
/*
        Template Name: Evènememt
    */
?>
<?php get_header() ?>

<?php while (have_posts()) : the_post(); ?>
    <?php
    $slides = ['slide1.jpg', "slide2.jpg", "slide3.jpg", "slide4.jpg"];
    $slide = $slides[rand(0, 3)];
    ?>
    <div class="nav-backed-header parallax" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/<?= $slide ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li> <a href="<?= bloginfo("url") ?>">Accueil</a></li>
                        <li> <a href="#">Agenda</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-8">
                    <h1>Agenda de <?= bloginfo("title") ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 contenu">
                        <?php $filter = [
                            [
                                'key' => 'date',
                                'value' => date('Y-m-d'),
                                'type' => 'date',
                                'compare' => '>='
                            ]
                        ]; ?>
                        <?php $blogs = new WP_Query(['post_type' => 'post', 'category_name' => 'evenements', 'post_status' => 'publish', 'meta_query' => $filter]); ?>
                        <?php if ($blogs->have_posts()) : ?>
                            <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
                                <article style="background-color: #f8f7f3; padding: 10px; margin: 10px;">
                                    <div class="row">
                                        <div class="col-sm-6 col-md-4">
                                            <figure style="height: 200px; overflow: hidden;">
                                                <img src="<?= the_post_thumbnail_url() ?>">
                                            </figure>
                                        </div>
                                        <div class="col-sm-6 col-md-8">
                                            <h4><?= the_title() ?></h4>
                                            <?= the_excerpt() ?>
                                            <section>
                                                <i class="fa fa-map-marker"></i> <?= the_field("lieu") ?>
                                                <i class="fa fa-calendar"></i> <?= the_field("date") ?>
                                                <a href="<?= the_permalink() ?>" class="btn btn-default btn-sm pull-right">Détail </a>
                                            </section>
                                        </div>
                                    </div>
                                </article>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                    <div class="col-md-4">
                        <?php get_template_part("templates/side") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_footer() ?>