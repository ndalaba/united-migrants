<?php
/*
        Template Name: Actualites
    */
?>
<?php get_header() ?>

<?php
$slides = ['slide1.jpg', "slide2.jpg", "slide3.jpg", "slide4.jpg"];
$slide = $slides[rand(0, 3)];
?>
<?php while (have_posts()) : the_post(); ?>
    <div class="nav-backed-header parallax" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/<?= $slide ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li> <a href="<?= bloginfo("url") ?>">Accueil</a></li>
                        <li> <a href="<?= site_url(getCatSlug(get_the_ID())) ?>"><?= getCatName(get_the_ID()) ?></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-8">
                    <h1><a href="<?= site_url(getCatSlug(get_the_ID())) ?>"><?= getCatName(get_the_ID()) ?></a></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 contenu">
                        <article class="post-content cause-item">
                            <h2 class="post-title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h2>
                            <?php if (has_post_thumbnail()) : ?>
                                <a href="<?= the_permalink() ?>" style="height: 230px;overflow: hidden;display: inline-block;"><img src="<?= the_post_thumbnail_url() ?>" class="img-responsive"></a>
                            <?php endif; ?>
                            <?php if (getCatSlug(get_the_ID()) != "faq") : ?>
                                <span class="meta-data">
                                    <span> <i class="fa fa-calendar"></i> <?= the_time('d F Y') ?></span>
                                    <span> <a href="#"><i class="fa fa-user"></i><?= the_author_meta("nicename") ?></a>
                                    </span>
                                </span>
                            <?php endif; ?>
                            <?php if (getCatSlug(get_the_ID()) != "faq") : ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <strong>Tous les détails</strong>
                                            </div>
                                            <div class="panel-body">
                                                <ul class="info-table">
                                                    <li><i class="fa fa-calendar"></i> <strong><?= the_field("date") ?></strong></li>
                                                    <li><i class="fa fa-clock-o"></i> <?= the_field("heure") ?></li>
                                                    <li><i class="fa fa-map-marker"></i> <?= the_field("lieu") ?></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="spacer-30"></div>
                            <?= the_content() ?>
                        </article>
                    </div>
                    <div class="col-md-4">
                        <?php get_template_part("templates/side") ?>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php get_template_part('templates/home_blog') ?>
            </div>
        </div>
    </div>
    <?php get_footer() ?>