/*global tarteaucitron */
tarteaucitron.lang = {
    "adblock": "Bonjour! Ce site joue la transparence et vous donne le choix des services tiers Ã  activer.",
    "adblock_call": "Merci de dÃ©sactiver votre adblocker pour commencer la personnalisation.",
    "reload": "Recharger la page",

    "alertBigScroll": "En continuant de dÃ©filer,",
    "alertBigClick": "En poursuivant votre navigation,",
    "alertBig": "vous acceptez l'utilisation de services tiers pouvant installer des cookies",

    "alertBigPrivacy": "Ce site utilise des cookies et vous donne le contrÃ´le sur ce que vous souhaitez activer",
    "alertSmall": "Gestion des services",
    "acceptAll": "OK, tout accepter",
    "personalize": "Personnaliser",
    "close": "Fermer",

    "all": "PrÃ©fÃ©rence pour tous les services",

    "info": "Protection de votre vie privÃ©e",
    "disclaimer": "En autorisant ces services tiers, vous acceptez le dÃ©pÃ´t et la lecture de cookies et l'utilisation de technologies de suivi nÃ©cessaires Ã  leur bon fonctionnement.",
    "allow": "Autoriser",
    "deny": "Interdire",
    "noCookie": "Ce service ne dÃ©pose aucun cookie.",
    "useCookie": "Ce service peut dÃ©poser",
    "useCookieCurrent": "Ce service a dÃ©posÃ©",
    "useNoCookie": "Ce service n'a dÃ©posÃ© aucun cookie.",
    "more": "En savoir plus",
    "source": "Voir le site officiel",
    "credit": "Gestion des cookies par tarteaucitron.js",

    "fallback": "est dÃ©sactivÃ©.",

    "ads": {
        "title": "RÃ©gies publicitaires",
        "details": "Les rÃ©gies publicitaires permettent de gÃ©nÃ©rer des revenus en commercialisant les espaces publicitaires du site."
    },
    "analytic": {
        "title": "Mesure d'audience",
        "details": "Les services de mesure d'audience permettent de gÃ©nÃ©rer des statistiques de frÃ©quentation utiles Ã  l'amÃ©lioration du site."
    },
    "social": {
        "title": "RÃ©seaux sociaux",
        "details": "Les rÃ©seaux sociaux permettent d'amÃ©liorer la convivialitÃ© du site et aident Ã  sa promotion via les partages."
    },
    "video": {
        "title": "VidÃ©os",
        "details": "Les services de partage de vidÃ©o permettent d'enrichir le site de contenu multimÃ©dia et augmentent sa visibilitÃ©."
    },
    "comment": {
        "title": "Commentaires",
        "details": "Les gestionnaires de commentaires facilitent le dÃ©pÃ´t de vos commentaires et luttent contre le spam."
    },
    "support": {
        "title": "Support",
        "details": "Les services de support vous permettent d'entrer en contact avec l'Ã©quipe du site et d'aider Ã  son amÃ©lioration."
    },
    "api": {
        "title": "APIs",
        "details": "Les APIs permettent de charger des scripts : gÃ©olocalisation, moteurs de recherche, traductions, ..."
    },
    "other": {
        "title": "Autre",
        "details": "Services visant Ã  afficher du contenu web."
    }
};