<?php
/*
        Template Name: Contact
    */
?>

<?php get_header() ?>

<?php while (have_posts()) : the_post(); ?>
    <?php
    $slides = ['slide1.jpg', "slide2.jpg", "slide3.jpg", "slide4.jpg"];
    $slide = $slides[rand(0, 3)];
    ?>
    <div class="nav-backed-header parallax" style="background-image:url(<?= get_template_directory_uri() ?>/assets/images/<?= $slide ?>);">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li> <a href="<?= bloginfo("url") ?>">Accueil</a></li>
                        <li> <a href="#"><?= the_title() ?></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-sm-10 col-xs-8">
                    <h1><?= the_title() ?></h1>
                </div>
            </div>
        </div>
    </div>

    <div class="main" role="main">
        <div id="content" class="content full">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 contenu">
                        <header class="single-post-header clearfix">
                            <h2 class="post-title">Nous trouver</h2>
                        </header>
                        <iframe src="<?= get_option('map') ?>" style="width: 100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <div class="col-md-5">
                        <h2 class="post-title" style="margin: 10px 0 15px;">Adresse</h2>
                        <h4 style="color: #4c4b4b;"><?= get_option("address") ?></h4>
                        <h5>Email: <a href="#"><?= get_option("email") ?></a> </h5>
                        <h5>Tel: <a><?= get_option("phone") ?></a> </h5>
                        <h5>Web: <a>www.united-migrants.org</a> </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endwhile; ?>
<?php get_template_part("templates/galerie") ?>
<?php get_footer() ?>

















<?php get_header() ?>

<div class="kl-slideshow static-content__slideshow scontent__maps">
    <div class="th-google_map" style="height: 700px;">
        <iframe src="<?= get_option('map') ?>" style="width: 100%" height="700" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>

    <div class="kl-contentmaps__panel" style="top: 150px">
        <a href="#" class="js-toggle-class kl-contentmaps__panel-tgg hidden-xs" data-target=".kl-contentmaps__panel" data-target-class="is-closed"></a>
        <!-- Image & Image pop-up -->
        <a href="<?= get_option('image_locaux') ?>" data-lightbox="image" class="kl-contentmaps__panel-img" style="background-image:url(<?= get_option('image_locaux') ?>)"></a>

        <!-- Content -->
        <div class="kl-contentmaps__panel-info">
            <h5 class="kl-contentmaps__panel-title">Nos locaux</h5>
            <div class="kl-contentmaps__panel-info-text">
                <p>
                    <?= get_option('description') ?>
                </p>
            </div>
        </div>
        <!--/ Content -->
    </div>
    <!-- Google map content panel -->
</div>

<section class="hg_section ptop-80 pbottom-80" style="padding-top: 120px">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <!-- Contact form -->
                <div class="contactForm">
                    <?php echo do_shortcode('[contact-form-7 id="99" title="Contact"]'); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <!-- Contact details -->
                <div class="text_box">
                    <h3 class="text_box-title text_box-title--style2">CONTACT</h3>

                    <p>
                        <?= nl2br(get_option('full_address')) ?>
                    </p>
                    <p>
                        <a href="mailto:<?= get_option('email') ?>"><?= get_option('email') ?></a><br>
                        <a href="https://www.litt-dev.com/">www.litt-dev.com</a>
                    </p>
                </div>
                <!--/ Contact details -->
            </div>
            <!--/ col-md-3 col-sm-3 -->
        </div>
        <!--/ .row -->
    </div>
    <!--/ .container -->
</section>


<?php get_footer() ?>