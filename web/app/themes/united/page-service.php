<?php
    /*
        Template Name: Service
    */
?>
<?php get_header() ?>

<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask6">
    <div class="bgback"></div>
    <div class="kl-bg-source">
        <div class="kl-video-container kl-bg-source__video">
            <div class="kl-video-wrapper video-grid-overlay">               
                <div class="kl-video valign halign" style="width: 100%; height: 100%;" ></div>
            </div>
        </div>
        <div class="kl-bg-source__overlay" style="background:rgba(130,36,227,0.3); background: -moz-linear-gradient(left, rgba(130,36,227,0.3) 0%, rgba(51,158,221,0.4) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(130,36,227,0.3)), color-stop(100%,rgba(51,158,221,0.4))); background: -webkit-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -o-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -ms-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: linear-gradient(to right, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); ">
        </div>
    </div>
    <div class="th-sparkles"></div>
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <ul class="breadcrumbs fixclear">
                            <li><a href="<?= site_url() ?>">Accueil</a></li>
                            <li><?= get_the_title()  ?></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-9">
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle">
                            <?= bloginfo('name') ?> CE QUE NOUS SAVONS FAIRE
                            </h2>                           
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kl-bottommask kl-bottommask--mask6">
        <svg width="2700px" height="57px" class="svgmask" viewBox="0 0 2700 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask6">
                    <feOffset dx="0" dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" in="shadowBlurOuter1" type="matrix" result="shadowMatrixOuter1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <g transform="translate(-1.000000, 10.000000)">
                <path d="M0.455078125,18.5 L1,47 L392,47 L1577,35 L392,17 L0.455078125,18.5 Z" fill="#000000"></path>
                <path d="M2701,0.313493752 L2701,47.2349598 L2312,47 L391,47 L2312,0 L2701,0.313493752 Z" fill="#f5f5f5" class="bmask-bgfill" filter="url(#filter-mask6)"></path>
                <path d="M2702,3 L2702,19 L2312,19 L1127,33 L2312,3 L2702,3 Z" fill="#cd2122" class="bmask-customfill"></path>
            </g>
        </svg>
    </div>
    <!--/ Sub-Header bottom mask style 6 -->
</div>

<section class="hg_section ptop-50">     
    <div class="container">
        <div class="row">				
            <div class="col-md-12 col-sm-12">
                <div class="text-left tbk-symbol--line tbk-icon-pos--after-title clearfix">
                    <h2 class="black fs-34 fw-semibold">NOS SERVICES</h2>
                    <div class="tbk__symbol ">
                        <span></span>
                    </div>
                    <h4 class="tbk__subtitle fs-30 light-gray fw-thin"><?= get_option('description') ?></h4>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hg_section">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="services_box services_box--modern sb--hasicon">
                    <div class="services_box__inner clearfix">
                        <div class="services_box__icon">
                            <div class="services_box__icon-inner">
                                <span class="services_box__fonticon icon-noun_65754"></span>
                            </div>
                        </div>
                        <div class="services_box__content">
                            <h4 class="services_box__title">APPLICATIONS WEB</h4>
                            <div class="services_box__desc">
                                <p>
                                    Quelle que soit votre problématique métier, nous vous accompagnons dans leur réalisation, en utilisant les meilleures solutions.
                                </p>
                            </div>
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Création de sites web </span></li>
                                    <li><span class="services_box__list-text">Développement spécifique et maintenance</span></li>
                                    <li><span class="services_box__list-text">Référencement web </span></li>
                                    <li><span class="services_box__list-text">Hébergement email et domaine professionel</span></li>                        
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="services_box services_box--modern sb--hasicon">
                    <div class="services_box__inner clearfix">
                        <div class="services_box__icon">
                            <div class="services_box__icon-inner">
                                <span class="services_box__fonticon icon-process2"></span>
                            </div>
                        </div>
                        <div class="services_box__content">
                            <h4 class="services_box__title">PUBLICATION NUMÉRIQUE</h4>
                            <div class="services_box__desc">
                                <p>
                                    Nous concevons des logos, des affiches et des brochures innovants. Une bonne efficience en termes de rapport coût/personnes touchées
                                </p>
                            </div>
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">Design de Logo</span></li>
                                    <li><span class="services_box__list-text">Flyers &amp; Brochures</span></li>
                                    <li><span class="services_box__list-text">Carte de visite</span></li>                        
                                    <li><span class="services_box__list-text">Présentation Powerpoint</span></li>                                
                                    <li><span class="services_box__list-text">Bannières</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="services_box services_box--modern sb--hasicon">
                    <div class="services_box__inner clearfix">
                        <div class="services_box__icon">
                            <div class="services_box__icon-inner">
                                <span class="services_box__fonticon icon-process3"></span>
                            </div>
                        </div>
                        <div class="services_box__content">
                            <h4 class="services_box__title">Marketing internet</h4>                    
                            <div class="services_box__desc">
                                <p>
                                Référencement naturel, publicitaire ou social, nous proposons des solutions e-marketing efficaces.
                                </p>
                            </div>
                            <div class="services_box__list-wrapper">
                                <span class="services_box__list-bg"></span>
                                <ul class="services_box__list">
                                    <li><span class="services_box__list-text">L'optimisation des moteurs de recherche</span></li>
                                    <li><span class="services_box__list-text">Marketing des moteurs de recherche</span></li>
                                    <li><span class="services_box__list-text">Publicité sur Internet</span></li>
                                    <li><span class="services_box__list-text">Analyse Web</span></li>
                                    <li><span class="services_box__list-text">Marketing de blog</span></li>
                                </ul>
                            </div> 
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('templates/home_partnaire') ?>

<section class="hg_section ptop-60 bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="callout-banner clearfix">
                    <div class="row">
                        <div class="col-sm-10">
                            <h3 class="m_title"><span class="fw-thin">Nous créons des applications ou sites web fonctionnels, 
                                <span class="fw-semibold"> modernes et robustes</span>.</span></h3>
                            <p style="font-size: 16px">Nos solutions sont équipées d'un espace d'administration ergonomique, optimisées pour le référencement naturel et sont compatibles avec les tablettes et mobiles</p>
                        </div>
                        <div class="col-sm-2">
                            <a href="<?= site_url('/nos-realisations') ?>" class="circlehover with-symbol style3" data-size="" data-position="top-left" data-align="right" target="_blank">
                                <span class="text">NOS 
                                    <span class="fw-normal">RÉFÉRENCES</span>
                                </span>
                                <span class="symbol">
                                    <img src="<?= get_template_directory_uri() ?>/assets/images/callout2.svg" alt="">
                                </span>
                                <div class="triangle">
                                    <span class="play-icon"></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>  



<?php get_footer() ?>