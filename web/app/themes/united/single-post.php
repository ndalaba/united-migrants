<?php get_header() ?>

<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask6">
    <div class="bgback"></div>
    <div class="kl-bg-source">
        <div class="kl-video-container kl-bg-source__video">
            <div class="kl-video-wrapper video-grid-overlay">
                <div class="kl-video valign halign" style="width: 100%; height: 100%;"></div>
            </div>
        </div>
        <div class="kl-bg-source__overlay" style="background:rgba(130,36,227,0.3); background: -moz-linear-gradient(left, rgba(130,36,227,0.3) 0%, rgba(51,158,221,0.4) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(130,36,227,0.3)), color-stop(100%,rgba(51,158,221,0.4))); background: -webkit-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -o-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -ms-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: linear-gradient(to right, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); ">
        </div>
    </div>
    <div class="th-sparkles"></div>
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8">
                        <ul class="breadcrumbs fixclear">
                            <li><a href="<?= site_url() ?>">Accueil</a></li>
                            <li><?= get_the_title()  ?></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-4">
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle">
                                Dernières actualités
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kl-bottommask kl-bottommask--mask6">
        <svg width="2700px" height="57px" class="svgmask" viewBox="0 0 2700 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask6">
                    <feOffset dx="0" dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" in="shadowBlurOuter1" type="matrix" result="shadowMatrixOuter1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <g transform="translate(-1.000000, 10.000000)">
                <path d="M0.455078125,18.5 L1,47 L392,47 L1577,35 L392,17 L0.455078125,18.5 Z" fill="#000000"></path>
                <path d="M2701,0.313493752 L2701,47.2349598 L2312,47 L391,47 L2312,0 L2701,0.313493752 Z" fill="#f5f5f5" class="bmask-bgfill" filter="url(#filter-mask6)"></path>
                <path d="M2702,3 L2702,19 L2312,19 L1127,33 L2312,3 L2702,3 Z" fill="#cd2122" class="bmask-customfill"></path>
            </g>
        </svg>
    </div>
</div>

<section class="hg_section ptop-50">
    <div class="container">
        <div class="row">
        <?php get_template_part('inc/news-sidebar') ?>
            <div class="col-md-9 col-sm-9">
                <div class="itemListView clearfix eBlog">
                    <div class="itemList">
                            <div class="itemContainer">
                                <div class="itemHeader">
                                    <h3 class="itemTitle">
                                        <a href="<?= the_permalink() ?>" title="<?= the_title() ?>"><?= the_title() ?></a>
                                    </h3>
                                    <div class="post_details">
                                        <span class="catItemDateCreated">
                                            <?= the_time('l, d F Y') ?> </span>
                                        <span class="catItemAuthor">Par <a href="#" title="Posts by Marius H." rel="author"><?= the_author_meta('nickname') ?></a></span>
                                    </div>
                                </div>
                                <div class="itemBody">
                                    <div class="itemIntroText">
                                        <div class="hg_post_image">
                                            <a href="<?= the_permalink() ?>" class="pull-left" title="<?= the_title() ?>">
                                                <img src="<?= get_template_directory_uri() ?>/assets/images/no_image.png" data-isrc="<?= the_post_thumbnail_url() ?>" class="" width="460" height="230" alt="<?= the_title() ?>" title="<?= the_title() ?>">
                                            </a>
                                        </div>
                                        <?= the_content() ?>
                                    </div>                                   
                                </div>
                                <ul class="itemLinks clearfix">
                                    <li class="itemCategory">
                                        <span class="glyphicon glyphicon-folder-close"></span>
                                        <span>Publié dans</span>
                                        <?= the_category(",","span") ?>
                                    </li>
                                </ul>
                                <div class="clear"> </div>
                            </div>
                            <div class="clear"></div>                       
                    </div>                  
                </div>
            </div>
           
        </div>
    </div>
</section>

<?php get_footer() ?>