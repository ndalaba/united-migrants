<?php
/*
        Template Name: Portfolio
    */
?>
<?php get_header() ?>

<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask6">
    <div class="bgback"></div>
    <div class="kl-bg-source">
        <div class="kl-video-container kl-bg-source__video">
            <div class="kl-video-wrapper video-grid-overlay">
                <div class="kl-video valign halign" style="width: 100%; height: 100%;"></div>
            </div>
        </div>
        <div class="kl-bg-source__overlay" style="background:rgba(130,36,227,0.3); background: -moz-linear-gradient(left, rgba(130,36,227,0.3) 0%, rgba(51,158,221,0.4) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(130,36,227,0.3)), color-stop(100%,rgba(51,158,221,0.4))); background: -webkit-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -o-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -ms-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: linear-gradient(to right, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); ">
        </div>
    </div>
    <div class="th-sparkles"></div>
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <ul class="breadcrumbs fixclear">
                            <li><a href="<?= site_url() ?>">Accueil</a></li>
                            <li>NOS RÉALISATIONS</li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-9">
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle">
                                Nous créons des applications web, mobiles; modernes et robustes.
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kl-bottommask kl-bottommask--mask6">
        <svg width="2700px" height="57px" class="svgmask" viewBox="0 0 2700 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask6">
                    <feOffset dx="0" dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" in="shadowBlurOuter1" type="matrix" result="shadowMatrixOuter1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <g transform="translate(-1.000000, 10.000000)">
                <path d="M0.455078125,18.5 L1,47 L392,47 L1577,35 L392,17 L0.455078125,18.5 Z" fill="#000000"></path>
                <path d="M2701,0.313493752 L2701,47.2349598 L2312,47 L391,47 L2312,0 L2701,0.313493752 Z" fill="#f5f5f5" class="bmask-bgfill" filter="url(#filter-mask6)"></path>
                <path d="M2702,3 L2702,19 L2312,19 L1127,33 L2312,3 L2702,3 Z" fill="#cd2122" class="bmask-customfill"></path>
            </g>
        </svg>
    </div>
</div>


<section class="hg_section ptop-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <?php
                $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                $portfolios = new WP_Query(['post_type' => 'portfolio', 'post_status' => 'publish', 'posts_per_page' => 6, 'paged' => $paged]);
                ?>
                <?php if ($portfolios->have_posts()) : ?>
                    <div class="row">
                        <?php while ($portfolios->have_posts()) : $portfolios->the_post(); ?>
                            <div class="col-sm-12">
                                <div class="portfolio-item kl-has-overlay">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="img-intro">
                                                <a href="<?= the_post_thumbnail_url() ?>" data-type="image" data-lightbox="image" class="hoverLink"></a>
                                                <img src="<?= get_template_directory_uri() ?>/assets/images/no_image.png" data-isrc="<?= the_post_thumbnail_url() ?>" class="img-responsive" title="<?= the_title() ?>" alt="<?= the_title() ?>">
                                                <div class="overlay">
                                                    <div class="overlay-inner">
                                                        <span class="glyphicon glyphicon-picture"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="portfolio-entry">
                                                <h3 class="title"><a href="<?= the_permalink() ?>"><?= the_title() ?></a></h3>
                                                <div class="pt-cat-desc">
                                                    <p>
                                                        <?= the_excerpt() ?>
                                                    </p>
                                                </div>
                                                <ul class="portfolio-item-details clearfix">
                                                    <li class="portfolio-item-details-client clearfix">
                                                        <span class="portfolio-item-details-label">CLIENT </span>
                                                        <span class="portfolio-item-details-item"><?= the_field('customer') ?></span>
                                                    </li>
                                                    <li class="portfolio-item-details-year clearfix">
                                                        <span class="portfolio-item-details-label">ANNÉE </span>
                                                        <span class="portfolio-item-details-item"><?= the_field('year') ?></span>
                                                    </li>

                                                    <li class="portfolio-item-details-cat clearfix">
                                                        <span class="portfolio-item-details-label">CATÉGORIES </span>
                                                        <span class="portfolio-item-details-item">
                                                            <?php global $post;
                                                            $terms = wp_get_post_terms($post->ID, 'domaine'); ?>
                                                            <?php if ($terms) : ?>
                                                                <?php foreach ($terms as $term) : ?>
                                                                    <a href="#" rel="tag"><?= $term->name ?></a>,
                                                                <?php endforeach; ?>
                                                            <?php endif ?>
                                                        </span>
                                                    </li>
                                                </ul>
                                                <div class="portfolio-item-otherdetails clearfix">
                                                    <div class="portfolio-item-share clearfix" data-share-title="PARTAGER:">
                                                        <a href="#" data-url="<?= the_permalink() ?>" title="PARTAGER SUR TWITTER" class=" portfolio-item-share-twitter tw-share">
                                                            <span class="icon-twitter"></span>
                                                        </a>
                                                        <a href="#" data-url="<?= the_permalink() ?>" title="PARTAGER SUR FACEBOOK" class="portfolio-item-share-facebook fb-share">
                                                            <span class="icon-facebook"></span>
                                                        </a>
                                                        <a href="#" data-url="<?= the_permalink() ?>" title="PARTAGER PAR MAIL" class=" portfolio-item-share-mail mail-share">
                                                            <span class="glyphicon glyphicon-envelope"></span>
                                                        </a>
                                                    </div>
                                                    <div class="portfolio-item-livelink">
                                                        <a class="btn btn-lined lined-custom" href="<?= the_field('url') ?>" target="_blank">
                                                            <span class="visible-sm visible-xs visible-lg">VOIR LE PROJET</span>
                                                            <span class="visible-md">PRÉVISUALISER</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        <?php endwhile; ?>
                    </div>
                    <?php $pagination= pagination_bar($portfolios); if ( ! empty( $pagination ) ) : ?>
                        <ul class="pagination">
                            <?php foreach ( $pagination as $key => $page_link ) : ?>
                                <li class="<?php if ( strpos( $page_link, 'current' ) !== false ) { echo ' active'; } ?>"><span><?=$page_link ?></span></li>
                            <?php endforeach ?>
                        </ul>
                    <?php endif; ?>
                <?php wp_reset_postdata();
                endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer() ?>