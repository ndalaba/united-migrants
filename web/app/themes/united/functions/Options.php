<?php

class Options
{
    const GROUP="site_options";

    public static function register()
    {
        add_action('admin_menu', [self::class,'addMenu']);
        add_action('admin_init',[self::class,'registerSettings']);
    }

    public static function registerSettings(){
        register_setting(self::GROUP,'description');
        register_setting(self::GROUP,'email');
        register_setting(self::GROUP,'phone');
        register_setting(self::GROUP,'address');
        register_setting(self::GROUP,'facebook');
        register_setting(self::GROUP,'twitter');
        register_setting(self::GROUP,'linkedin');
        register_setting(self::GROUP,'map');
        register_setting(self::GROUP,'image_locaux');
        register_setting(self::GROUP,'full_address');
    }

    public static function addMenu()
    {
        add_options_page("Options du site", "Options", 'manage_options', self::GROUP, [self::class,'render']);        
    }

    public static function render()
    {
        ?>     
            <div class="wrap">
                <h1>Options du site</h1>
                <form method="post" action="options.php">
                    <?php settings_fields(self::GROUP); ?>
                    <table class="form-table" role="presentation">
                        <tbody>
                            <tr>
                                <th scope="row"><label for="description">Description</label></th>
                                <td><textarea name="description" id="description" class="large-text code"><?= get_option('description') ?></textarea></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="email">Contact email</label></th>
                                <td><input name="email" type="email" id="email" value="<?= get_option('email') ?>" class="large-text code"></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="phone">Téléphone</label></th>
                                <td><input name="phone" type="text" id="phone" value="<?= get_option('phone') ?>" class="large-text code"></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="address">Adresse</label></th>
                                <td><textarea name="address" id="address" class="large-text code"><?= get_option('address') ?></textarea></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="full_address">Adresse complète</label></th>
                                <td><textarea name="full_address" id="full_address" class="large-text code"><?= get_option('full_address') ?></textarea></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="map">Map</label></th>
                                <td><textarea name="map" id="map" class="large-text code"><?= get_option('map') ?></textarea></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="image_locaux">Image locaux</label></th>
                                <td><input name="image_locaux" type="text" id="image_locaux" value="<?= get_option('image_locaux') ?>" class="large-text"></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="facebook">Facebook</label></th>
                                <td><input name="facebook" type="text" id="facebook" value="<?= get_option('facebook') ?>" class="large-text"></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="twitter">Twitter</label></th>
                                <td><input name="twitter" type="text" id="twitter" value="<?= get_option('twitter') ?>" class="large-text"></td>
                            </tr>
                            <tr>
                                <th scope="row"><label for="linkedin">Linked In</label></th>
                                <td><input name="linkedin" type="text" id="linkedin" value="<?= get_option('linkedin') ?>" class="large-text"></td>
                            </tr>
                        </tbody>
                    </table>
                    <?php submit_button() ?>
                </form>
            </div>
       
      <?php
    }
}
