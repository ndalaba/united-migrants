<?php


class DomaineType{

    public static function register(){
        add_action('init',[self::class,'postType']);
    }

    public static function postType(){
        $labels = array(
            'name'                       => _x('Domaines', 'Taxonomy General Name', 'text_domain'),
            'singular_name'              => _x('Domaine', 'Taxonomy Singular Name', 'text_domain'),
            'menu_name'                  => __('Domaine', 'text_domain'),
            'all_items'                  => __('Tous les domaines', 'text_domain'),
            'parent_item'                => __('null', 'text_domain'),
            'parent_item_colon'          => __('null', 'text_domain'),
            'new_item_name'              => __('Ajouter un domaine', 'text_domain'),
            'add_new_item'               => __('Nouveau domaine', 'text_domain'),
            'edit_item'                  => __('Modifier un domaine', 'text_domain'),
            'update_item'                => __('Modifier un domaine', 'text_domain'),
            'view_item'                  => __('Voir le domaine', 'text_domain'),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'show_in_rest'               => true
        );
        register_taxonomy('domaine', ['service'], $args);
    }
}