<?php

remove_action('wp_head', 'wp_generator');

add_action('init', function () {
    register_nav_menus(['header' => "Menu header page d'accueil"]);
});

add_action('after_setup_theme', function () {
    add_theme_support('title-tag');
    add_theme_support('post-thumbnails');
});


function pagination_bar($custom_query)
{

    $total_pages = $custom_query->max_num_pages;
    $big = 999999999; // need an unlikely integer

    if ($total_pages > 1) {
        $current_page = max(1, get_query_var('paged'));

        $pagination = paginate_links(array(
            'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
            'format' => '?paged=%#%',
            'current' => $current_page,
            'total' => $total_pages,
            'type' => 'array'
        ));
        return $pagination;
    }
}

//require_once('functions/DomaineType.php');
require_once('functions/Options.php');
require_once('functions/ServiceType.php');

//DomaineType::register();
Options::register();
ServiceType::register();