<footer>
	<div class="footer__area footer-bg box-105">
		<div class="container-fluid">
			<div class="footer__bottom pt-130 pb-50">
				<div class="row">
					<div class="col-xl-3 col-lg-6 col-md-6 mb-50">
						<div class="footer__widget">
							<div class="award__slider grey-bg-2 text-center" style="padding: 40px 50px;margin-bottom: 10px;-webkit-border-radius: 10px;-moz-border-radius: 10px;    border-radius: 10px;">
								<h1><?= bloginfo("title") ?> </h1>
							</div>
							<div class="footer__widget-content">
								<div class="footer__logo-area" style="padding-left: 20px;">
									<p><?= get_option("description") ?></p>
									<div class="social">
										<h4>Nous suivre</h4>
										<ul>
											<li>
												<a href="<?= get_option('facebook') ?>">
													<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-facebook" width="30" height="30" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
														<path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
														<path d="M7 10v4h3v7h4v-7h3l1 -4h-4v-2a1 1 0 0 1 1 -1h3v-4h-3a5 5 0 0 0 -5 5v2h-3"></path>
													</svg>
												</a>
											</li>
											<li>
												<a href="<?= get_option('twitter') ?>">
													<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-twitter" width="30" height="30" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
														<path stroke="none" d="M0 0h24v24H0z" fill="none" />
														<path d="M22 4.01c-1 .49 -1.98 .689 -3 .99c-1.121 -1.265 -2.783 -1.335 -4.38 -.737s-2.643 2.06 -2.62 3.737v1c-3.245 .083 -6.135 -1.395 -8 -4c0 0 -4.182 7.433 4 11c-1.872 1.247 -3.739 2.088 -6 2c3.308 1.803 6.913 2.423 10.034 1.517c3.58 -1.04 6.522 -3.723 7.651 -7.742a13.84 13.84 0 0 0 .497 -3.753c-.002 -.249 1.51 -2.772 1.818 -4.013z" />
													</svg>
												</a>
											</li>
											<li>
												<a href="<?= get_option('linkedin') ?>">
													<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-linkedin" width="30" height="30" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
														<path stroke="none" d="M0 0h24v24H0z" fill="none" />
														<rect x="4" y="4" width="16" height="16" rx="2" />
														<line x1="8" y1="11" x2="8" y2="16" />
														<line x1="8" y1="8" x2="8" y2="8.01" />
														<line x1="12" y1="16" x2="12" y2="11" />
														<path d="M16 16v-3a2 2 0 0 0 -4 0" />
													</svg>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-6 col-md-6 offset-xl-1 mb-50">
						<div class="footer__widget">
							<div class="footer__widget-title mb-25">
								<h2>Services</h2>
							</div>
							<div class="footer__widget-content">
								<div class="footer__services">
									<ul>

										<li><a href="services-details.html">Mental Problem</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-2 col-lg-6 col-md-6 mb-50">
						<div class="footer__widget">
							<div class="footer__widget-title mb-25">
								<h2>Nous contacter</h2>
							</div>
							<div class="footer__widget-content">
								<div class="footer__contact-info">
									<ul>
										<li>
											<div class="footer__contact-address">
												<span><?= get_option("address") ?></span>
											</div>
										</li>
										<li>
											<div class="footer__contact-item">
												<h6>Email:</h6>
												<p><a href="#" class="__cf_email__"><?= get_option("email") ?></a></p>
											</div>
										</li>
										<li>
											<div class="footer__contact-item">
												<h6>Tel:</h6>
												<p><?= get_option("phone") ?></p>
											</div>
										</li>
										<li>
											<div class="footer__contact-item">
												<h6>Web:</h6>
												<p>www.united-migrants.org</p>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-lg-6 col-md-6 offset-xl-1 mb-50">
						<div class="footer__widget">
							<div class="footer__widget-title mb-25">
								<h2>Stay in Touch</h2>
							</div>
							<div class="footer__widget-content">
								<div class="footer__subscribe">
									<p>Fruitful and herb the seasons of fish saying likeness face beast cattle.</p>
									<div class="footer__subscribe-form">
										<form action="#">
											<input type="email" placeholder="Email Address">
											<button class="s-btn" type="submit"><i class="fal fa-paper-plane"></i> subscribe now</button>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer__copyright">
				<div class="row">
					<div class="col-sm-6">
						<div class="footer__copyright-text">
							<p>Copyright © <?= date('Y') ?> tous droits reservés. <a href="#"> <?= bloginfo("title") ?></a>.</p>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="footer__policy ">
							<a href="<?= site_url('/qui-sommes-nous') ?>">Nous connaitre</a>
							<a href="#"> Privacy</a>
							<a href="#">Support</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>