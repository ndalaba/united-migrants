<div class="slider-area p-relative" style="background-image: linear-gradient(to right top, #272828, #4d4f4f, #787979, #a5a6a6, #b6bdc1);">
    <div class="slider-bg-text p-absolute">
        <h2 class="vert-move">UM</h2>
    </div>
    <div class="container-fluid p-0">
        <div class="slider-active">
            <?php $blogs = new WP_Query(['post_type' => 'post', 'post_status' => 'publish', 'posts_per_page' => 2]); ?>
            <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
                <div class="single-slider slider-height d-flex align-items-center">
                    <div class="row align-items-center no-gutters mt-40">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="slider-content">
                                <!--<span data-animation="fadeInUp" data-delay=".2s"><?= the_excerpt() ?></span>-->
                                <h2 data-animation="fadeInUp" data-delay=".4s"><?= substr(the_title(),0,70)?></h2>
                                <div class="slider-btn" data-animation="fadeInUp" data-delay=".6s">
                                    <a href="<?= the_permalink() ?>" class="s-btn s-btn__white">Voir</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 d-none d-md-inline-block">
                            <div class="slider-thumb text-right" data-animation="fadeInRight" data-delay=".8s">
                                <img src="<?= the_post_thumbnail_url() ?>" alt="<?= the_title() ?>">
                            </div>
                        </div>
                    </div>
                </div>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </div>
    </div>
</div>