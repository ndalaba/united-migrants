<header>
	<div class="header-area">
		<div class="header__top box-105">
			<div class="container-fluid">
				<div class="row align-items-center">
					<div class="col-xl-6 col-lg-6 col-md-4 col-sm-6 d-none d-sm-block">
						<div class="header__top-left">
							<div class="social">
								<ul>
									<li>
										<a href="<?= get_option('facebook') ?>">
											<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-facebook" width="30" height="30" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
												<path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
												<path d="M7 10v4h3v7h4v-7h3l1 -4h-4v-2a1 1 0 0 1 1 -1h3v-4h-3a5 5 0 0 0 -5 5v2h-3"></path>
											</svg>
										</a>
									</li>
									<li>
										<a href="<?= get_option('twitter') ?>">
											<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-twitter" width="30" height="30" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
												<path stroke="none" d="M0 0h24v24H0z" fill="none" />
												<path d="M22 4.01c-1 .49 -1.98 .689 -3 .99c-1.121 -1.265 -2.783 -1.335 -4.38 -.737s-2.643 2.06 -2.62 3.737v1c-3.245 .083 -6.135 -1.395 -8 -4c0 0 -4.182 7.433 4 11c-1.872 1.247 -3.739 2.088 -6 2c3.308 1.803 6.913 2.423 10.034 1.517c3.58 -1.04 6.522 -3.723 7.651 -7.742a13.84 13.84 0 0 0 .497 -3.753c-.002 -.249 1.51 -2.772 1.818 -4.013z" />
											</svg>
										</a>
									</li>
									<li>
										<a href="<?= get_option('linkedin') ?>">
											<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-brand-linkedin" width="30" height="30" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
												<path stroke="none" d="M0 0h24v24H0z" fill="none" />
												<rect x="4" y="4" width="16" height="16" rx="2" />
												<line x1="8" y1="11" x2="8" y2="16" />
												<line x1="8" y1="8" x2="8" y2="8.01" />
												<line x1="12" y1="16" x2="12" y2="11" />
												<path d="M16 16v-3a2 2 0 0 0 -4 0" />
											</svg>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-8 col-sm-6 col-12">
						<div class="header__top-right d-flex justify-content-end">
							<div class="header__info d-none d-md-flex">
								<div class="header__info-icon">
									<span class="icon flaticon-24-hours-support"></span>
								</div>
								<div class="header__info-text">
									<span>Tel:</span>
									<span><?= get_option('phone') ?></span>
								</div>
							</div>
							<div class="header__info d-none d-sm-flex">
								<div class="header__info-icon">
									<span class="icon flaticon-envelope"></span>
								</div>
								<div class="header__info-text">
									<span>e-mail:</span>
									<span><?= get_option('email') ?></span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="header-sticky" class="header__bottom box-105">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-2 col-lg-3 col-md-4 col-sm-6 col-8">
						<div class="logo-2 text-md-center">
							<a href="<?= bloginfo("url") ?>"><img src="<?= get_template_directory_uri() ?>/assets/images/logo_footer.png" alt="<?= bloginfo("title") ?>" style="height: 87px;"></a>
						</div>
					</div>
					<div class="col-xl-10 col-lg-9 col-md-8 col-sm-6 col-4">
						<div class="header__bottom-right pink-soft-bg d-flex justify-content-end justify-content-lg-between align-items-center">
							<?php if (has_nav_menu('header')) : ?>
								<?php wp_nav_menu(['theme_location' => "header", 'container_class' => 'main-menu main-menu-2 d-none d-lg-block', 'container_id' => 'main-menu', 'menu_id' => 'menu-main-menu', 'menu_class' => 'nav-menu']) ?>
							<?php endif; ?>

							<div class="header-bar info-toggle-btn d-lg-none f-right">
								<span></span>
								<span></span>
								<span></span>
							</div>
							<div class="header__btn d-none d-xl-block">
								<a href="contact.html" class="s-btn s-btn__square">Contact Us Now</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>