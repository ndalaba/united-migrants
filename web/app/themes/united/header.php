<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="Diallo Mamadou N'Dalaba">
	<?php wp_head(); ?>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;700&display=swap" rel="stylesheet">
	<link rel="icon" type="image/png" href="<?= get_template_directory_uri() . '/assets/images/logo_white.png' ?>" sizes="16x16">
	
			<!-- CSS here -->
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/preloader.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/bootstrap.min.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/slick.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/metisMenu.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/owl.carousel.min.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/animate.min.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/all.min.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/flaticon.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/nice-select.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/default.css' ?>">
	<link rel="stylesheet" href="<?= get_template_directory_uri() . '/assets/css/style.css' ?>">
	
</head>

<body>
	<!--<div id="loading">
		<div id="loading-center">
				<div id="loading-center-absolute">
					<div class="object" id="object_one"></div>
					<div class="object" id="object_two"></div>
					<div class="object" id="object_three"></div>
				</div>
		</div> 
	</div>-->

	<?php get_template_part('inc/header') ?>
	
			