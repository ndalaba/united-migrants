<section class="case__area pt-110 pb-90 mb-345 pink-soft-bg">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 offset-xl-2">
                <div class="section__title mb-80 text-center">
                    <div class="section__icon mb-10">
                        <span class="section__sub-title section__sub-title-2">Actualités</span>
                    </div>
                    <h1 class="mb-100">Dernières infos migrants</h1>
                </div>
            </div>
        </div>
        <div class="row">
            <?php $blogs = new WP_Query(['post_type' => 'post','category_name'=>'actualites', 'post_status' => 'publish', 'posts_per_page' => 3]); ?>
            <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="case__item-2 mb--328 p-relative">
                        <div class="case__thumb-2 w-img" style="height: 210px;overflow: hidden;">
                            <img src="<?= the_post_thumbnail_url() ?>" alt="<?php the_title() ?>">
                        </div>
                        <div class="case__content-2 transition-3 grey-bg-3 p-relative">
                            <h3 style="height: 80px;"><a href="<?= the_permalink() ?>"><?= substr(the_title('','',false),0,70) ?>...</a></h3>
                            <!--<?= the_excerpt() ?>-->
                            <a href="<?= the_permalink() ?>" class="link-btn">Lire</a>
                        </div>
                    </div>
                </div>
            <?php endwhile; wp_reset_postdata(); ?>
        </div>
    </div>
</section>