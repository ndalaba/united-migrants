<section class="faq__area p-relative box-105 pink-soft-bg pt-110 pb-120" style="position: relative;z-index:20">
    <div class="faq__bg p-absolute" data-background="<?= get_template_directory_uri() ?>/assets/images/service.jpg"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 col-lg-12">
                <div class="faq__wrapper pr-140">
                    <div class="section__title mb-20">
                        <div class="section__icon mb-10">
                            <span class="section__sub-title section__sub-title-2">Nos actions</span>
                        </div>
                        <h1 class="mb-100">Ce que nous pouvons faire pour vous</h1>
                    </div>
                    <div class="faq__inner mr-30">
                        <div id="accordion">
                            <?php $services = new WP_Query(['post_type' => 'service', 'post_status' => 'publish', 'posts_per_page' => 3]); ?>
                            <?php if ($services->have_posts()) : ?>
                                <?php while ($services->have_posts()) : $services->the_post(); ?>
                                    <div class="card">
                                        <div class="card-header white-bg" id="heading<?= the_ID() ?>">
                                            <h5 class="mb-0">
                                                <button class="faq-accordion-btn collapsed" data-toggle="collapse" data-target="#collapse<?= the_ID() ?>" aria-expanded="false" aria-controls="collapse<?= the_ID() ?>">
                                                    <?= the_title() ?>
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapse<?= the_ID() ?>" class="collapse " aria-labelledby="heading<?= the_ID() ?>" data-parent="#accordion">
                                            <div class="card-body">
                                                <a href="<?= the_permalink() ?>"> <?= the_excerpt() ?></a>
                                            </div>
                                        </div>
                                    </div>
                            <?php endwhile;
                            endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>