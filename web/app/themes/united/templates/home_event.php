<section class="blog__area pt-105 pb-90" style="background-color: #f1f2f2;">
   <div class="container">
      <div class="row">
         <div class="col-xl-8 offset-xl-2">
            <div class="section__title mb-70 text-center pl-50 pr-50">
               <h1>Les évènements de <?= bloginfo("title") ?></h1>
               <div class="section__icon mb-10">
                  <span class="section__sub-title section__sub-title-2 section__sub-title-3 section__sub-title-4">Évènements</span>
               </div>
            </div>
         </div>
      </div>
      <div class="row">

         <?php $blogs = new WP_Query(['post_type' => 'post', 'category_name' => 'evenements', 'post_status' => 'publish', 'posts_per_page' => 3]); ?>
         <?php while ($blogs->have_posts()) : $blogs->the_post(); ?>
            <div class="col-xl-4 col-lg-4 col-md-6">
               <div class="blog__item mb-30">
                  <div class="blog__thumb blog__thumb-3 w-img fix" style="height: 270px;">
                     <img src="<?= the_post_thumbnail_url() ?>" alt="<?php the_title() ?>"/>
                  </div>
                  <div class="blog__content blog__content-3 pink-soft-bg p-relative">
                     <div class="blog__meta blog__date-3 p-absolute fix">
                        <div class="blog__date ">
                           <h6><?= the_date("d") ?></h6>
                           <span><?= get_the_date("M") ?></span>
                        </div>
                     </div>
                     <h2 style="height: 100px;"><a href="<?= the_permalink() ?>"><?= substr(the_title('', '', false), 0, 70) ?>...</a></h2>
                     <a href="<?= the_permalink() ?>" class="link-btn">Lire</a>
                  </div>
               </div>
            </div>
         <?php endwhile;
         wp_reset_postdata(); ?>

      </div>
   </div>
</section>