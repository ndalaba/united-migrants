<div data-vc-full-width="true" data-vc-full-width-init="false" class="themestek-row wpb_row vc_row-fluid vc_custom_1611746981754 vc_row-has-fill themestek-total-col-1 themestek-zindex-0 themestek-textcolor-white themestek-bgimage-position-center_center themestek-bg themestek-bgimage-yes">
   <div class="themestek-row-wrapper-bg-layer themestek-bg-layer"></div><!-- ThemeStek custom DIV added -->
   <div class="vc_row container">
      <div class="themestek-column wpb_column vc_column_container vc_col-sm-12 themestek-zindex-0">
         <div class="vc_column-inner  ">
            <div class="wpb_wrapper">
               <div class="themestek-row-inner vc_row wpb_row vc_inner vc_row-fluid themestek-zindex-0">
                  <div class="themestek-column-inner wpb_column vc_column_container vc_col-sm-5 themestek-zindex-0">
                     <div class="vc_column-inner  ">

                        <div class="wpb_wrapper">
                           <div class="themestek-element-heading-wrapper themestek-heading-inner themestek-element-align-left themestek-seperator-none ">
                              <section class="themestek-vc_cta3-container">
                                 <div class="themestek-vc_general themestek-vc_cta3 themestek-cta3-only themestek-vc_cta3-style-classic themestek-vc_cta3-shape-rounded themestek-vc_cta3-align-left themestek-vc_cta3-color-transparent themestek-vc_cta3-icon-size-md themestek-vc_cta3-actions-no themestek-cta3-without-desc">
                                    <div class="themestek-vc_cta3_content-container">
                                       <div class="themestek-vc_cta3-content">
                                          <div class="themestek-vc_cta3-content-header themestek-wrap">
                                             <div class="themestek-vc_cta3-headers themestek-wrap-cell">
                                                <?php if (getLang() == 'en') : ?>
                                                   <h4 class="themestek-custom-heading ">MEET OUR STUFF</h4>
                                                   <h2 class="themestek-custom-heading ">Advice on a full range of <br /> questions</h2>
                                                <?php endif ?>
                                                <?php if (getLang() == 'fr') : ?>
                                                   <h4 class="themestek-custom-heading ">RENCONTREZ NOS ÉQUIPES</h4>
                                                   <h2 class="themestek-custom-heading ">Conseils sur une gamme <br />complète de questions.</h2>
                                                <?php endif ?>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                           </div> <!-- .themestek-element-heading-wrapper container -->
                        </div>
                     </div>
                  </div>
                  <div class="themestek-column-inner wpb_column vc_column_container vc_col-sm-7 themestek-zindex-0">
                     <div class="vc_column-inner vc_custom_1576754080144 themestek-responsive-custom-84856910">

                        <div class="wpb_wrapper">

                           <div class="wpb_text_column wpb_content_element  vc_custom_1576754089610">
                              <div class="wpb_wrapper">
                                 <?php if (getLang() == "en") : ?>
                                    <p>Lalya Notaire's team is made up of notaries who share a common philosophy and values.
                                       Fully involved, this multidisciplinary team mobilizes skills,
                                       know-how, sense of anticipation and availability at the service of its customers, by offering them permanent assistance</p>
                                 <?php endif; ?>
                                 <?php if (getLang() == "fr") : ?>
                                    <p>L'équipe de Lalya nOtaire est composée de notaires partageant une philosophie et des valeurs communes.
                                       Totalement impliquée, cette équipe pluridisciplinaire mobilise compétence,
                                       savoir-faire, sens de l'anticipation et disponibilité au service de ses clients, en leur offrant une assistance permanente.</p>
                                 <?php endif; ?>
                              </div>
                           </div>

                           <div class="themestek-vc_btn3-container themestek-vc_btn3-inline vc_custom_1576754098713">
                              <a class="themestek-vc_general themestek-vc_btn3 themestek-vc_btn3-size-md themestek-vc_btn3-shape-rounded themestek-vc_btn3-style-text themestek-vc_btn3-weight-yes themestek-vc_btn3-icon-right themestek-vc_btn3-color-white" href="" title="">
                                 <?php if (getLang() == "en") : ?>
                                    Join Our Team
                                 <?php endif; ?>
                                 <?php if (getLang() == "fr") : ?>
                                    Notre équipe
                                 <?php endif; ?>
                                 <i class="themestek-vc_btn3-icon themifyicon ti-arrow-right"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="themestek-boxes themestek-boxes-team themestek-boxes-view-default themestek-boxes-col-four themestek-boxes-sortable-no themestek-boxes-textalign-center themestek-boxes-sortablebutton- themestek-two-colum-view vc_custom_1577853387297">
                  <div class="themestek-boxes-inner themestek-boxes-team-inner ">
                     <div class="row multi-columns-row themestek-boxes-row-wrapper">
                        <?php $equipes = new WP_Query(['post_type' => 'equipe', 'post_status' => 'publish', 'posts_per_page' => 4, 'orderby' => 'ID', 'order' => 'ASC']); ?>
                        <?php if ($equipes->have_posts()) : ?>
                           <?php while ($equipes->have_posts()) : $equipes->the_post(); ?>
                              <div class="themestek-box-col-wrapper col-lg-3 col-sm-6 col-md-3 col-xs-12 ">
                                 <article class="themestek-box themestek-box-team themestek-teambox-style-3">
                                    <div class="themestek-post-item">
                                       <div class="themestek-team-image-box">
                                          <span class="themestek-item-thumbnail">
                                             <span class="themestek-item-thumbnail-inner">
                                                <img width="600" height="700" src="<?= the_post_thumbnail_url([270, 270]) ?>" class="attachment-themestek-img-600x700 size-themestek-img-600x700 wp-post-image" alt="<?= get_post_field('name') ?>">
                                             </span>
                                          </span>
                                          <div class="themestek-teambox-social-links">
                                             <div class="themestek-team-social-links-wrapper">
                                                <ul class="themestek-team-social-links">
                                                   <li>
                                                      <a href="<?= get_post_field('facebook') ?>" target="_blank" class="themestek-team-social-facebook">
                                                         <i class="themestek-attorco-icon-facebook"></i>
                                                         <span class="themestek-hide">Facebook</span>
                                                      </a>
                                                   </li>
                                                   <li>
                                                      <a href="<?= get_post_field('twitter') ?>" target="_blank" class="themestek-team-social-twitter">
                                                         <i class="themestek-attorco-icon-twitter"></i>
                                                         <span class="themestek-hide">Twitter</span>
                                                      </a>
                                                   </li>

                                                   <li>
                                                      <a href="<?= get_post_field('linkedin') ?>" target="_blank" class="themestek-team-social-linkedin">
                                                         <i class="themestek-attorco-icon-linkedin"></i>
                                                         <span class="themestek-hide">LinkedIn</span>
                                                      </a>
                                                   </li>
                                                </ul> <!-- .themestek-team-social-links -->
                                             </div> <!-- .themestek-team-social-links-wrapper -->
                                          </div>
                                       </div>
                                       <div class="themestek-box-content">
                                          <div class="themestek-box-content-inner">
                                             <div class="themestek-pf-box-title">
                                                <div class="themestek-box-title">
                                                   <h4><a href="#"><?= get_post_field('name') ?></a></h4>
                                                </div>
                                             </div>
                                             <div class="themestek-box-team-position themestek-skincolor"><?= get_the_title() ?></div>
                                          </div>
                                       </div>
                                    </div>
                                 </article>
                              </div>
                           <?php endwhile;
                           wp_reset_postdata(); ?>
                        <?php endif; ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>