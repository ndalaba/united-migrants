<?php
    /*
        Template Name: Contact
    */
?>
<?php get_header() ?>

<div class="kl-slideshow static-content__slideshow scontent__maps">
    <div class="th-google_map" style="height: 700px;">
        <iframe src="<?= get_option('map')?>" style="width: 100%" height="700" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>    
    </div>

    <div class="kl-contentmaps__panel" style="top: 150px">
        <a href="#" class="js-toggle-class kl-contentmaps__panel-tgg hidden-xs" data-target=".kl-contentmaps__panel" data-target-class="is-closed"></a>
        <!-- Image & Image pop-up -->
        <a href="<?= get_option('image_locaux') ?>" data-lightbox="image" class="kl-contentmaps__panel-img" style="background-image:url(<?= get_option('image_locaux') ?>)"></a>

        <!-- Content -->
        <div class="kl-contentmaps__panel-info">
            <h5 class="kl-contentmaps__panel-title">Nos locaux</h5>
            <div class="kl-contentmaps__panel-info-text">
                <p>
                    <?= get_option('description') ?>
                </p>
            </div>
        </div>
        <!--/ Content -->
    </div>
    <!-- Google map content panel -->
</div>

<section class="hg_section ptop-80 pbottom-80" style="padding-top: 120px">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <!-- Contact form -->
                <div class="contactForm">
                <?php echo do_shortcode( '[contact-form-7 id="99" title="Contact"]' ); ?>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <!-- Contact details -->
                <div class="text_box">
                    <h3 class="text_box-title text_box-title--style2">CONTACT</h3>
                                        
                    <p>
                        <?= nl2br(get_option('full_address')) ?>
                    </p>
                    <p>
                        <a href="mailto:<?= get_option('email') ?>"><?= get_option('email') ?></a><br>
                        <a href="https://www.litt-dev.com/">www.litt-dev.com</a>
                    </p>
                </div>
                <!--/ Contact details -->
            </div>
            <!--/ col-md-3 col-sm-3 -->
        </div>
        <!--/ .row -->
    </div>
    <!--/ .container -->
</section>


<?php get_footer() ?>