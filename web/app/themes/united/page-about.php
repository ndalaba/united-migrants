<?php
    /*
        Template Name: About
    */
?>
<?php get_header() ?>

<div id="page_header" class="page-subheader site-subheader-cst uh_zn_def_header_style maskcontainer--mask6">
    <div class="bgback"></div>
    <div class="kl-bg-source">
        <div class="kl-video-container kl-bg-source__video">
            <div class="kl-video-wrapper video-grid-overlay">               
                <div class="kl-video valign halign" style="width: 100%; height: 100%;" >        </div>
            </div>
        </div>
        <div class="kl-bg-source__overlay" style="background:rgba(130,36,227,0.3); background: -moz-linear-gradient(left, rgba(130,36,227,0.3) 0%, rgba(51,158,221,0.4) 100%); background: -webkit-gradient(linear, left top, right top, color-stop(0%,rgba(130,36,227,0.3)), color-stop(100%,rgba(51,158,221,0.4))); background: -webkit-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -o-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: -ms-linear-gradient(left, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); background: linear-gradient(to right, rgba(130,36,227,0.3) 0%,rgba(51,158,221,0.4) 100%); ">
        </div>
    </div>
    <div class="th-sparkles"></div>
    <div class="ph-content-wrap">
        <div class="ph-content-v-center">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <ul class="breadcrumbs fixclear">
                            <li><a href="<?= site_url() ?>">Accueil</a></li>
                            <li><?= get_the_title()  ?></li>
                        </ul>
                       <!-- <span id="current-date" class="subheader-currentdate"><?= get_the_date() ?></span>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-6">
                        <div class="subheader-titles">
                            <h2 class="subheader-maintitle"><?= get_the_title()  ?></h2>
                            <h4 class="subheader-subtitle">EN SAVOIR PLUS SUR NOUS</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="kl-bottommask kl-bottommask--mask6">
        <svg width="2700px" height="57px" class="svgmask" viewBox="0 0 2700 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask6">
                    <feOffset dx="0" dy="-2" in="SourceAlpha" result="shadowOffsetOuter1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetOuter1" result="shadowBlurOuter1"></feGaussianBlur>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.5 0" in="shadowBlurOuter1" type="matrix" result="shadowMatrixOuter1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="shadowMatrixOuter1"></feMergeNode>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <g transform="translate(-1.000000, 10.000000)">
                <path d="M0.455078125,18.5 L1,47 L392,47 L1577,35 L392,17 L0.455078125,18.5 Z" fill="#000000"></path>
                <path d="M2701,0.313493752 L2701,47.2349598 L2312,47 L391,47 L2312,0 L2701,0.313493752 Z" fill="#f5f5f5" class="bmask-bgfill" filter="url(#filter-mask6)"></path>
                <path d="M2702,3 L2702,19 L2312,19 L1127,33 L2312,3 L2702,3 Z" fill="#cd2122" class="bmask-customfill"></path>
            </g>
        </svg>
    </div>
    <!--/ Sub-Header bottom mask style 6 -->
</div>

<section class="hg_section ptop-80 pbottom-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="kl-title-block clearfix text-left tbk-symbol-- tbk-icon-pos--after-title">
                    <h3 class="tbk__title montserrat fw-bold fs-28"><?= bloginfo('title')?></h3>
                    <h4 class="tbk__subtitle fw-vthin fs-18 lh-32"><?= get_option('description') ?></h4>
                </div>
                <div class="hg_separator clearfix mb-65"></div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default">
                            <div class="kl-iconbox__inner">
                                <div class="kl-iconbox__icon-wrapper ">
                                    <img class="kl-iconbox__icon" src="<?= get_template_directory_uri() ?>/assets/images/no_image.png" data-isrc="<?= get_template_directory_uri().'/assets/' ?>images/ib-ico-12.svg" alt="APPLICATION WEB">
                                </div>
                                <div class="kl-iconbox__content-wrapper">
                                    <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                                        <h3 class="kl-iconbox__title">Applications web</h3>
                                    </div>
                                    <div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                                        <p class="kl-iconbox__desc">
                                            Quelle que soit votre problématique métier, nous vous accompagnons dans leur réalisation, en utilisant les meilleures solutions.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default">
                            <div class="kl-iconbox__inner">
                                <div class="kl-iconbox__icon-wrapper">
                                    <img class="kl-iconbox__icon" src="<?= get_template_directory_uri() ?>/assets/images/no_image.png" data-isrc="<?= get_template_directory_uri().'/assets/' ?>images/ib-ico-21.svg" alt="GRAPHIC DESIGN">
                                </div>
                                <div class="kl-iconbox__content-wrapper">
                                    <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                                        <h3 class="kl-iconbox__title">Publication numérique</h3>
                                    </div>
                                    <div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                                        <p class="kl-iconbox__desc">
                                        Nous concevons des logos, des affiches et des brochures innovants. Une bonne efficience en termes de rapport coût/personnes touchées
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default ">
                            <div class="kl-iconbox__inner">
                                <div class="kl-iconbox__icon-wrapper ">
                                    <img class="kl-iconbox__icon" src="<?= get_template_directory_uri() ?>/assets/images/no_image.png" data-isrc="<?= get_template_directory_uri().'/assets/' ?>images/ib-ico-4.svg" alt="SEO SERVICES">
                                </div>
                                <div class="kl-iconbox__content-wrapper">
                                    <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                                        <h3 class="kl-iconbox__title">Hébergement Web</h3>
                                    </div>
                                    <div class="kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                                        <p class="kl-iconbox__desc">
                                            Un hébergement performant, stable, sécurisé et bien administré est la fondation de tous projets web.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="kl-iconbox kl-iconbox--align-left text-left kl-iconbox--theme-default ">
                            <div class="kl-iconbox__inner">
                                <div class="kl-iconbox__icon-wrapper">
                                   <img class="kl-iconbox__icon" src="<?= get_template_directory_uri() ?>/assets/images/no_image.png" data-isrc="<?= get_template_directory_uri().'/assets/' ?>images/ib-ico-5.svg" alt="MARKETING">
                                </div>
                                <div class="kl-iconbox__content-wrapper">
                                    <div class="kl-iconbox__el-wrapper kl-iconbox__title-wrapper">
                                        <h3 class="kl-iconbox__title">Marketing internet</h3>
                                    </div>
                                    <div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
                                        <p class="kl-iconbox__desc">
                                        Référencement naturel, publicitaire ou social, nous proposons des solutions e-marketing efficaces.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="th-spacer clearfix" style="height: 60px;"> </div>
                <div id="skills_diagram_el" class="kl-skills-diagram">
                    <div class="kl-skills-legend legend-topright">
                        <h4>LEGENDE</h4>
                        <ul class="kl-skills-list">
                            <li data-percent="80" style="background-color:#97be0d;">Javascript</li>
                            <li data-percent="90" style="background-color:#d84f5f;">SEO/SEM</li>
                            <li data-percent="77" style="background-color:#6eabe5;">Wordpress</li>
                            <li data-percent="64" style="background-color:#8dc9e8;">Design</li>
                            <li data-percent="70" style="background-color:#841c1c;">PHP </li>
                            <li data-percent="65" style="background-color:#0e3c5b;">Python </li>
                        </ul>
                    </div>
                    <div class="skills-responsive-diagram">
                        <div id="thediagram" class="kl-diagram" data-width="600" data-height="600" data-maincolor="#193340" data-maintext="Compétences" data-fontsize="20px Arial" data-textcolor="#ffffff"></div>
                    </div>
                </div>
                <script type="text/javascript" src="<?= get_template_directory_uri().'/assets/' ?>js/raphael-min.js"></script>
                <script type="text/javascript" src="<?= get_template_directory_uri().'/assets/' ?>js/init.js"></script>
            </div>
        </div>
    </div>
</section>

<section class="hg_section p-0">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="hg_separator clearfix mb-65"></div>
            </div>
        </div>
    </div>
</section>

<section class="hg_section">
    <div class="full_width">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="recentwork_carousel recentwork_carousel--2 clearfix">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="recentwork_carousel__left">
                                <h3 class="recentwork_carousel__title m_title">QUELQUES RÉALISATIONS</h3>
                                <p class="recentwork_carousel__desc">
                                    Quelques conceptions de sites internet, d'applications web, mobiles....
                                </p>
                                <a href="<?= site_url('/nos-realisations') ?>" class="btn btn-fullcolor">TOUT VOIR</a>
                                <div class="controls recentwork_carousel__controls">
                                    <a href="#" class="prev recentwork_carousel__prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                                    <a href="#" class="next recentwork_carousel__next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="recentwork_carousel__crsl-wrapper">
                                <ul class="recent_works1 fixclear recentwork_carousel__crsl">                                 
                                    <?php get_template_part('inc/work')?>                               
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="hg_section--relative p-0">
    <div class="full_width">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="media-container style2 h-400">
                    <div class="kl-bg-source">
                        <div class="kl-bg-source__bgimage" style="background-image:url(<?= get_template_directory_uri().'/assets/images/bg1.jpg' ?>);background-repeat:no-repeat;background-attachment:scroll;background-position-x:center;background-position-y:center;background-size:cover"></div>
                        <div class="kl-bg-source__overlay" style="background-color:rgba(0,0,0,0.65)"></div>
                        <div class="kl-bg-source__overlay-gloss"></div>
                    </div>
                    <a class="media-container__link media-container__link--btn media-container__link--style-borderanim2 " href="#" target="_self">
                        <div class="borderanim2-svg">
                            <svg height="70" width="400" xmlns="http://www.w3.org/2000/svg">
                            <rect class="borderanim2-svg__shape" height="70" width="400"></rect>
                            </svg>
                            <span class="media-container__text"><?= bloginfo('title') ?></span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="kl-bottommask kl-bottommask--mask3">
        <svg width="5000px" height="57px" class="svgmask " viewBox="0 0 5000 57" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs>
                <filter x="-50%" y="-50%" width="200%" height="200%" filterUnits="objectBoundingBox" id="filter-mask3">
                    <feOffset dx="0" dy="3" in="SourceAlpha" result="shadowOffsetInner1"></feOffset>
                    <feGaussianBlur stdDeviation="2" in="shadowOffsetInner1" result="shadowBlurInner1"></feGaussianBlur>
                    <feComposite in="shadowBlurInner1" in2="SourceAlpha" operator="arithmetic" k2="-1" k3="1" result="shadowInnerInner1"></feComposite>
                    <feColorMatrix values="0 0 0 0 0   0 0 0 0 0   0 0 0 0 0  0 0 0 0.4 0" in="shadowInnerInner1" type="matrix" result="shadowMatrixInner1"></feColorMatrix>
                    <feMerge>
                        <feMergeNode in="SourceGraphic"></feMergeNode>
                        <feMergeNode in="shadowMatrixInner1"></feMergeNode>
                    </feMerge>
                </filter>
            </defs>
            <path d="M9.09383679e-13,57.0005249 L9.09383679e-13,34.0075249 L2418,34.0075249 L2434,34.0075249 C2434,34.0075249 2441.89,33.2585249 2448,31.0245249 C2454.11,28.7905249 2479,11.0005249 2479,11.0005249 L2492,2.00052487 C2492,2.00052487 2495.121,-0.0374751261 2500,0.000524873861 C2505.267,-0.0294751261 2508,2.00052487 2508,2.00052487 L2521,11.0005249 C2521,11.0005249 2545.89,28.7905249 2552,31.0245249 C2558.11,33.2585249 2566,34.0075249 2566,34.0075249 L2582,34.0075249 L5000,34.0075249 L5000,57.0005249 L2500,57.0005249 L1148,57.0005249 L9.09383679e-13,57.0005249 Z" class="bmask-bgfill" filter="url(#filter-mask3)" fill="#f5f5f5"></path>
        </svg>
        <i class="glyphicon glyphicon-chevron-down"></i>
    </div>
</section>

<section class="hg_section ptop-80 pbottom-80">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="kl-title-block clearfix tbk--text-dark tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title">
                    <h3 class="tbk__title montserrat fs-28 fw-extrabold">STATISTIQUES &amp; FAITS</h3>
                    <span class="tbk__symbol "><span></span></span>
                </div>
                <?php get_template_part('inc/stat') ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer() ?>