<?php
/*
        Template Name: Home
    */
?>
<?php get_header() ?>
   <main>
      <!-- page title area start -->
      <section class="page__title p-relative pt-200 pb-200" data-background="<?= get_template_directory_uri() ?>/assets/images/actions.jpg">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 col-lg-12 col-md-12">
                  <div class="page__title-inner text-center">
                     <h1>Humanitaire - Caritative</h1>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- page title area end -->

      <!-- about area start -->
      <?php get_template_part("templates/home_about") ?>
      <?php get_template_part("templates/home_service") ?>
      <?php get_template_part("templates/home_blog") ?>
      <?php get_template_part("templates/home_event") ?>
   </main>
<?php get_footer() ?>