<?php get_template_part("inc/footer") ?>

<script src="<?= get_template_directory_uri() . '/assets/js/vendor/modernizr-3.5.0.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/vendor/jquery-1.12.4.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/vendor/waypoints.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/popper.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/bootstrap.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/metisMenu.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/slick.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/isotope.pkgd.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/owl.carousel.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/jquery.nice-select.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/wow.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/imagesloaded.pkgd.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/jquery.counterup.min.js' ?>"></script>
<script src="<?= get_template_directory_uri() . '/assets/js/main.js' ?>"></script>

<?php wp_footer() ?>

</body>

</html>